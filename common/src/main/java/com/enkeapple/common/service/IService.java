package com.enkeapple.common.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IService<E> {
    E create(E entity);

    E update(E entity);

    Page<E> findAll(Pageable pageable);

    default boolean delete(Long id) {
        return true;
    }

    default E findById(Long id) {
        return null;
    };
}
