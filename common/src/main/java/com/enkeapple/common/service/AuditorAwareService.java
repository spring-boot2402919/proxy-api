package com.enkeapple.common.service;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareService implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of("system");
    }
}
