package com.enkeapple.common.service;

import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bind.annotation.*;

import java.lang.reflect.Method;

@Slf4j
public class CacheInterceptor {
    @RuntimeType
    public static Object intercept(@This Object self,
                                   @Origin Method method,
                                   @AllArguments Object[] args,
                                   @SuperMethod(nullIfImpossible = true) Method superMethod,
                                   @Empty Object defaultValue) throws Throwable {
        return superMethod == null ? defaultValue : superMethod.invoke(self, args);
    }
}
