package com.enkeapple.common.service;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.util.ByteBuddyUtil;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.TypeCache;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PROTECTED)
public class CacheService<E extends BaseEntity> {
    final TypeCache<Class<? extends E>> cache = new TypeCache.WithInlineExpunction<>(TypeCache.Sort.WEAK);

    final ConcurrentHashMap<Class<?>, Object> map = new ConcurrentHashMap<>();

    public E findOrInsert(E entity) {
        return createProxy(entity, false);
    }

    public void insert(E entity) {
        createProxy(entity, true);
    };

    private E createProxy(E entity, boolean forceInsert) {
        Class<E> entityClass = (Class<E>) entity.getClass();

        try {
            Class<?> proxyClass = forceInsert
                    ? cache.insert(entityClass.getClassLoader(), entityClass, createByteBuddyType(entityClass))
                    : cache.findOrInsert(entityClass.getClassLoader(), entityClass, () -> createByteBuddyType(entityClass));

            E proxy = (E) Objects.requireNonNull(proxyClass).getDeclaredConstructor().newInstance();

            copyFields(entity, proxy);

            return proxy;
        } catch (Exception ex) {
            throw new RuntimeException("Failed to create proxy instance", ex);
        }
    }

    public void clearCache() {
        cache.expungeStaleEntries();
    }

    private Class<?> createByteBuddyType(Class<E> entityClass) {
        return new ByteBuddy()
                .subclass(entityClass)
                .method(ElementMatchers.any())
                .intercept(MethodDelegation.to(CacheInterceptor.class))
                .make()
                .load(entityClass.getClassLoader())
                .getLoaded()
                .asSubclass(entityClass);
    }

    private void copyFields(E source, E target) {
        Class<?> clazz = source.getClass();

        while (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);

                try {
                    field.set(target, field.get(source));
                } catch (IllegalAccessException e) {
                    log.error("Failed to copy fields from source to target", e);
                    throw new RuntimeException("Failed to copy fields", e);
                }
            }

            clazz = clazz.getSuperclass();
        }
    }
}
