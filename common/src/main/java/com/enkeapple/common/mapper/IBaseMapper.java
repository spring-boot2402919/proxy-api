package com.enkeapple.common.mapper;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.dto.BaseDTO;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

public interface IBaseMapper<E extends BaseEntity, D extends BaseDTO> {
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    D entityToDTO(E entity);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    E dtoToEntity(D dto);

    default PageMapper<D> convertPageToMap(Page page) {
        return PageMapper.builder()
                .total(page.getTotalElements())
                .page(page.getNumber())
                .size(page.getSize())
                .data(page.getContent())
                .build();
    }
}
