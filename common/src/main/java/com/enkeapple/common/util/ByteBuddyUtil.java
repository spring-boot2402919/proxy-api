package com.enkeapple.common.util;

import java.lang.reflect.Field;

public class ByteBuddyUtil {
    public static boolean isProxy(Object object) {
        return object != null && object.getClass().getSimpleName().contains("ByteBuddy$");
    }

    public static Object getOriginalObject(Object proxy) {
        if (isProxy(proxy)) {
            try {
                Field field = proxy.getClass().getDeclaredField("instance");

                field.setAccessible(true);

                return field.get(proxy);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
