package com.enkeapple.common.controller;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.dto.BaseDTO;
import com.enkeapple.common.mapper.PageMapper;
import com.enkeapple.common.mapper.IBaseMapper;
import com.enkeapple.common.service.BaseService;
import com.enkeapple.common.service.CacheService;
import com.enkeapple.common.util.ByteBuddyUtil;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class BaseController<E extends BaseEntity, D extends BaseDTO> {
    private static final Logger log = LoggerFactory.getLogger(BaseController.class);
    final BaseService<E> service;
    final IBaseMapper<E, D> mapper;

    @Autowired
    private CacheService<E> cacheService;

    @Autowired
    protected BaseController(BaseService<E> service, IBaseMapper<E, D> mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping({"", ""})
    public ResponseEntity<PageMapper<D>> getAll(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "5") Integer size
    ) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

        Page<D> result = service.findAll(pageable).map(mapper::entityToDTO);

        PageMapper<D> response = mapper.convertPageToMap(result);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<D> findById(@PathVariable long id) {
        E entity = service.findById(id);

        if (entity != null) {
            E proxy = cacheService.findOrInsert(entity);

            boolean isProxy = ByteBuddyUtil.isProxy(proxy);

            return ResponseEntity.ok(mapper.entityToDTO(isProxy ? proxy : entity));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping({"", ""})
    public ResponseEntity<D> save(@Valid @RequestBody D body) {
        E entity = service.create(mapper.dtoToEntity(body));

        cacheService.insert(entity);

        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.entityToDTO(entity));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable long id) {
        boolean deleted = service.delete(id);

        if (deleted) {
            cacheService.clearCache();

            return new ResponseEntity<>("Object with ID " + id + " has been successfully deleted.", HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable long id, @Valid @RequestBody D body) {
        E entity = mapper.dtoToEntity(body);

        entity.setId(id);

        service.update(entity);

        cacheService.insert(entity);

        return new ResponseEntity<>("Object with ID " + id + " has been successfully updated.", HttpStatus.OK);
    }
}
