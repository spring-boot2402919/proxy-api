package com.enkeapple.common.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" })
public class BaseDTO implements Serializable {
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  long id;
}
