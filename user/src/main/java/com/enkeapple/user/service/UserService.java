package com.enkeapple.user.service;

import com.enkeapple.common.service.BaseService;
import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService extends BaseService<UserEntity> {
    public UserService(UserRepository userRepository) {
        super(userRepository);
    }
}
