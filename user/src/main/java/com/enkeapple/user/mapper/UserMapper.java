package com.enkeapple.user.mapper;

import com.enkeapple.common.mapper.IBaseMapper;
import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.UserDTO;
import org.mapstruct.*;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserMapper extends IBaseMapper<UserEntity, UserDTO> {
    UserDTO entityToDTO(UserEntity entity);

    UserEntity dtoToEntity(UserDTO dto);
}
