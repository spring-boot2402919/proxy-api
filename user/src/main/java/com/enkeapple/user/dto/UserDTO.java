package com.enkeapple.user.dto;

import com.enkeapple.common.dto.BaseDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" })
public class UserDTO extends BaseDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    long id;

    String firstName;

    String lastName;
}